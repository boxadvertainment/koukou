<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;

class AppController extends Controller
{
    public function home()
    {
        return view('home');
    }

    public function friends()
    {
        return view('friends');
    }

    public function game()
    {
        return view('game');
    }

    public function products()
    {
        return view('products');
    }
}
