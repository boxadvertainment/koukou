var gulp        = require('gulp');
var elixir      = require('laravel-elixir');
var browserSync = require('browser-sync').create('My server');
var dotenv      = require('dotenv').load();

var Task = elixir.Task;

elixir.config.browserSync.proxy = process.env.APP_URL;

elixir.extend('browserSync', function () {
    /* _____________________________________________________________________________________ */
    // Add these lines to node_modules/laravel-elixir/tasks/shared/Css.js
    // after .pipe(gulp.dest(options.output.baseDir))
    // =====================================================================================
    // .pipe(require('browser-sync').get('My server').stream({match: '**/*.css'}))
    /* _____________________________________________________________________________________ */

    var src = [
        'app/**/*',
        'public/**/*',
        'resources/views/**/*',
        '!public/css/*'
    ];

    new Task('browserSync', function() {
        if (browserSync.active === true) {
            browserSync.reload();
        }
    }).watch(src);
});

elixir(function(mix) {
    mix
        .rubySass('main.scss', 'public/css/main.css')
        .babel([
          //'facebookUtils.js',
          'main.js'
        ], 'public/js/main.js')
        .scripts([
            'jquery/dist/jquery.min.js',
            'sweetalert/dist/sweetalert.min.js',
            'bootstrap/dist/js/bootstrap.min.js',
            'gsap/src/minified/TimelineLite.min.js',
            'gsap/src/minified/TweenMax.min.js',
            'animatesprite/scripts/jquery.animateSprite.min.js',
        ], 'public/js/vendor.js', 'vendor/bower_components' )
        .styles([
            'sweetalert/dist/sweetalert.css',
            'bootstrap/dist/css/bootstrap.min.css',
            'font-awesome/css/font-awesome.min.css',
            'animate.css/animate.min.css',
        ], 'public/css/vendor.css', 'vendor/bower_components')
        .copy('vendor/bower_components/modernizr/modernizr.js', 'public/js/modernizr.js')
        .copy('vendor/bower_components/font-awesome/fonts', 'public/fonts')
        //.version(['css/main.css', 'js/main.js'])
        .browserSync();
});
