@extends('layout')

@section('class', 'friends')

@section('content')
<div id="friends-section">
  <div class="container">
    <div class="slogan text-hide">إكتشف عالم الطيور مع كوكو و إربح رحلة أحلامك</div>
    <div class="friends-board">
      <div class="friends-wrap">
        <span class="bird bird-gris" data-bird="gris" data-video="//www.youtube.com/embed/zcNP70eknUA" data-audio="audio/gris.mp3"></span>
        <span class="bird bird-pingouin" data-bird="pingouin" data-video="//www.youtube.com/embed/Y5jR1sHNOwI" data-audio="audio/pingouin.mp3"></span>
        <span class="bird bird-perruche" data-bird="perruche" data-video="//www.youtube.com/embed/3tesoNA1nWU" data-audio="audio/perruche.mp3"></span>
        <span class="bird bird-flamant" data-bird="flamant" data-video="//www.youtube.com/embed/X1o9E9STfT8" data-audio="audio/flamant.mp3"></span>
        <span class="bird bird-cacatoes" data-bird="cacatoes" data-video="//www.youtube.com/embed/MOCPURZIWwk" data-audio="audio/cacatoes.mp3"></span>
        <span class="bird bird-toucan" data-bird="toucan" data-video="//www.youtube.com/embed/BUsEQoF407w" data-audio="audio/toucan.mp3"></span>

        <div class="friends-modal animated bounceIn hide">
          <button class="btn return-btn"></button>
          <div class="row">
            <div class="col-md-6">
              <div class="content content-gris">
                <span class="bird bird-gris"></span>
                <div class="description">
                  <p>Le perroquet Gris du Gabon est la plus grande et la plus populaire des espèces de perroquets d’Afrique.</p>
                  <p>Il se distingue par une couleur grise et une queue rouge.</p>
                  <p>Il était déjà un oiseau de compagnie dans la Rome antique.</p>
                  <p>La femelle pend de 3 à 4 œufs dont l'éclosion a lieu après 30 jours.</p>
                  <p>Cet animal vit en groupes de plusieurs centaines d'individus.</p>
                  <p>Le Gris du Gabon vit en moyenne 50 ans, et peut dépasser les 65 ans.</p>

                </div>
              </div>

              <div class="content content-pingouin">
                <span class="bird bird-pingouin"></span>
                <div class="description">
                  <p>Il est muni de deux ailes plates qu'il utilise pour nager. Il vit en mer ou à proximité des courants froids, et ne revient sur terre que pour se reproduire et élever ses poussins.</p>
                  <p>Il mange du poisson et ne sait pas voler. Il existe dix-sept espèces de Pingouin, le plus grand est le Pingouin empereur.</p>
                  <p>Les Pingouins vivent en moyenne 15 à 20 ans.</p>

                </div>
              </div>

              <div class="content content-perruche">
                <span class="bird bird-perruche"></span>
                <div class="description">
                  <p>Les Perruches sont souvent élevées en captivité pour la beauté de leur plumage.</p>
                  <p>Les Perruches couvrent toute la planète. Elle marquent une présence importante dans le continent Européen. </p>
                  <p>Elles ont de nombreuses couleurs variées, comme : le vert, le violet, le gris…Les Perruches sont faciles à élever et feront la joie de leur maitre.</p>
                  <p>Elles peuvent avoir plus de 3 000 plumes sur leur corps !</p>
                  <p>La Perruche à collier se nourrit essentiellement de fruits et de graines. </p>
                  <p>Elle a une durée de vie d'environ de 5 à 8 ans, mais si on en prend bien soin, certaines Perruches peuvent vivre jusqu'à 15 ans.</p>
                </div>
              </div>

              <div class="content content-flamant">
                <span class="bird bird-flamant"></span>
                <div class="description">
                  <p>Le Flamant Rose est l'un des plus beaux oiseaux que nous puissions observer, avec son plumage flamboyant et ses petits yeux jaunes vifs.</p>
                  <p>Il mesure entre 1,20 mètre et 1,40 mètre. Lorsqu'il déploie ses ailes, dont le dessous est rouge, il fait entre 1,30 mètre et 1,50 mètre. Le mâle est plus grand que la femelle.</p>
                  <p>Ses ailes sont puissantes et lui permettent de voler à 60 km/h sur plusieurs centaines de kilomètres. Mais il faut qu'il prenne beaucoup d'élan pour pouvoir s'envoler.</p>
                  <p>Il est tout léger, entre 2,5 kg et 3,5 kg. Il vit en moyenne 50 ans, ce qui est beaucoup pour un oiseau. Son cri ressemble à celui d'une oie.</p>
                  <p>Lorsqu'il dort il replie une patte sous lui, et enfouie sa tête sous une aile. C'est en fait pour réchauffer et reposer une de ses pattes (celle qu'il met sous ses plumes).</p>
                  <p>Il est monogame et ne peut avoir qu’un ou deux œufs par an. </p>

                </div>
              </div>

              <div class="content content-cacatoes">
                <span class="bird bird-cacatoes"></span>
                <div class="description">
                  <p>Ces perroquets sont des animaux de compagnie très prisés. Ils peuvent vivre de 50 à 80 ans et demandent beaucoup d’attention. </p>
                  <p>La reproduction a lieu d'août à janvier dans le Sud et de mai à septembre dans le Nord. La femelle pond de un à trois œufs dans un nid fait par les deux parents dans une cavité d'un arbre. Les œufs sont couvés trente jours à tour de rôle et les jeunes restent au nid 60 à 70 jours avant de pouvoir voler. Ils resteront avec leurs parents pendant leur première année d'existence.</p>
                  <p>Les couples sont fidèles avec une longévité estimée à 40 ans.</p>

                </div>
              </div>

              <div class="content content-toucan">
                <span class="bird bird-toucan"></span>
                <div class="description">
                  <p>Le Toucan est arboricole, c'est à dire qu'il vit dans les arbres. Il saute de branche en branche pour trouver sa nourriture qui se compose de fruits, d'oeufs, de petits oiseaux, d'insectes et de très petits mammifères. Il aime bien vivre en groupe.</p>
                  <p>Le Toucan est un oiseau qui a un bec énorme qui lui donne une allure bizarre. Mais ce bec d'oiseau est léger contrairement à ce que l'on pourrait penser.</p>
                  <p>La durée de vie du Toucan est de 15 à 20 ans.</p>
                </div>
              </div>

            </div>
            <div class="col-md-6">
              <audio class="bird-audio" id="bird-audio" controls>
                Your browser does not support the audio element.
              </audio>
              <hr class="sep">
              <div class="bird-video" class="embed-responsive embed-responsive-16by9">
                  <iframe class="embed-responsive-item" width="100%" height="300px" src="" frameborder="0" allowfullscreen></iframe>
              </div>
            </div>
          </div>


        </div>
      </div>
    </div>
  </div>

  @include('partials.footer')
</div>
@endsection
