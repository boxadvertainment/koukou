@extends('layout')

@section('class', 'game')

@section('content')

<div id="game-section">
    <div class="container noselect">
      <div class="slogan text-hide">إكتشف عالم الطيور مع كوكو و إربح رحلة أحلامك</div>
      <div id="koukou"></div>
      <div id="koukou-text"></div>
      <div class="game-board">
        <div class="game-wrap map disabled">
          <video class="map-game" width="826" height="530" poster="{{ asset('img/cover.jpg') }}" autoplay loop>
              <source src="{{ asset('map_animation.mp4') }}" type="video/mp4">
          </video>
          <div class="overlay"></div>
          <div class="holder-wrapper">
              <div class="holder" data-name="place1"></div>
              <div class="holder" data-name="place2"></div>
              <div class="holder" data-name="place3"></div>
              <div class="holder" data-name="place4"></div>
              <div class="holder" data-name="place5"></div>
              <div class="holder" data-name="place6"></div>
          </div>
        </div>
      </div>
      <div class="game-birds">
        <span class="friend not-placed bird bird-gris" data-name="bird1"></span>
        <span class="friend not-placed bird bird-pingouin" data-name="bird2"></span>
        <span class="friend not-placed bird bird-perruche" data-name="bird3"></span>
        <span class="friend not-placed bird bird-flamant" data-name="bird4"></span>
        <span class="friend not-placed bird bird-cacatoes" data-name="bird5"></span>
        <span class="friend not-placed bird bird-toucan" data-name="bird6"></span>
      </div>
    </div>
    @include('partials.footer')
</div>
@endsection

@section('body')
<div id="play-modal">
  <div class="content">
      <div class="close-btn"></div>
      <img src="{{ asset('img/Comment-jouer.png')}}" alt="">
  </div>
</div>
@endsection
