@extends('layout')

@section('class', 'home')

@section('content')
<section id="landing-section">
  <div class="container">
    <div class="slogan text-hide">إكتشف عالم الطيور مع كوكو و إربح رحلة أحلامك</div>

    <div class="globe-container">
      <div id="clouds"></div>
      <div id="globe"></div>
      <a href="{{ action('AppController@game') }}" id="play-btn" class="btn animated">Jouer</a>
      <a href="{{ action('AppController@products') }}" id="products-btn" class="btn animated">Nos produits</a>
      <a href="{{ action('AppController@friends') }}" id="friends-btn" class="btn animated">Les amis de Koukou</a>
      <button id="koukou-btn" class="btn animated" data-toggle="modal" data-target="#video-modal">Koukou l'aventurier</button>
      <button id="planning-btn" class="btn">Le planning de la tournée</button>
    </div>
  </div>
  @include('partials.footer', ['homeLink' => false])
</section>

<div id="planning-section" class="hide">
  <div class="container">
    <div class="planning-date">
      <div class="day"></div>
      <div class="date"></div>
    </div>
    <div class="slogan text-hide">إكتشف عالم الطيور مع كوكو و إربح رحلة أحلامك</div>
    <div class="planning-map">
      <div class="map">
        <!-- <div class="pin pin1">
          <div class="location">Bizerte</div>
          <div class="time">Matin</div>
          <div class="address">Parc d’attraction ALADIN</div>
        </div>
        <div class="pin pin2">
          <div class="location">Bizerte</div>
          <div class="time">Après-midi</div>
          <div class="address">Parc errif</div>
        </div> -->
      </div>
      <div class="pin animated bounce pin-tmpl">
        <div class="location"></div>
        <div class="time"></div>
        <div class="address"></div>
      </div>
      <button class="prev-btn btn">Rdv précédent</button>
      <button class="next-btn btn">Prochain rdv</button>
    </div>
  </div>

  @include('partials.footer')
</div>


<div id="video-modal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-lg bounceIn">
      <div class="modal-content">
          <div class="modal-body">
              <button class="close-btn btn" data-dismiss="modal" aria-label="Close"></button>
              <div class="video-player">
                <div class="embed-responsive embed-responsive-16by9">
                    <iframe class="embed-responsive-item" width="100%" height="300px" src="" frameborder="0" allowfullscreen></iframe>
                </div>
              </div>
          </div>
      </div>
  </div>
</div>
@endsection

@section('scripts')
<script>
  (function($){

    if (Modernizr.audio) {
      var ambientMusic = new Audio(); ambientMusic.src = 'sounds/music.mp3';
      ambientMusic.volume= 0.2;
      ambientMusic.loop = true;
      ambientMusic.play();
    };

    $('.sound-btn').click(function(){
      if ($('span',this).attr('class') == 'active') {
        ambientMusic.pause();
      }else{
        ambientMusic.play();
      };

      $('span',this).toggleClass('active');
    });

  })(jQuery);
</script>
@endsection
