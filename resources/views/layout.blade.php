<!doctype html>
<!--[if lt IE 9]>      <html class="no-js lt-ie9" lang="{{ App::getLocale() }}"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="{{ App::getLocale() }}"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>@yield('title'){{ Config::get('app.name') }}</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- <meta name="author" content="box.agency" /> -->

    @include('partials.socialMetaTags', [
        'title' => 'Découvrez Koukou et ses amis avec Tropico',
        'description' => ''
    ])

    <!-- Metta CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}" />

    <link rel="apple-touch-icon" href="apple-touch-icon.png">
    <!-- Place favicon.ico in the root directory -->

    <link rel="stylesheet" href="{{ asset('css/vendor.css') }}">
    <link rel="stylesheet" href="{{ asset('css/main.css') }}">

    @yield('styles')

    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <script>
        BASE_URL    = '{{ url('/') }}';
        CURRENT_URL = '{{ Request::url() }}';
        var today   = '{{ (new DateTime())->format('Y-m-d') }}'

    </script>
    <script src="{{ asset('js/modernizr.js') }}"></script>
</head>
<body class="@yield('class')">

    <!--[if lt IE 9]>
    <div class="alert alert-dismissible outdated-browser show" role="alert">
        <h6>Votre navigateur est obsolète !</h6>
        <p>Pour afficher correctement ce site et bénéficier d'une expérience optimale, nous vous recommandons de mettre à jour votre navigateur.
            <a href="http://outdatedbrowser.com/fr" class="update-btn" target="_blank">Mettre à jour maintenant </a>
        </p>
        <a href="#" class="close-btn" title="Fermer" data-dismiss="alert" aria-label="Fermer">&times;</a>
    </div>
    <![endif]-->

    <div id="leaves">
      <div class="leave1"></div>
      <div class="leave2"></div>
      <div class="leave3"></div>
      <div class="leave4"></div>
    </div>
    <div class="logo-wrapper">
      <div class="container">
        <h1 id="logo"><a href="{{ url('/') }}" class="text-hide">Tropico</a></h1>
        <button class="sound-btn" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Musique">
            <i class="fa fa-music"></i>
            <span class="active"></span>
        </button>
      </div>
    </div>

    @yield('content')

    @yield('body')

    <script src="{{ asset('js/vendor.js') }}"></script>
    <script src="{{ asset('js/main.js') }}"></script>

    @yield('scripts')

    <!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
    <script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
      ga('create', 'UA-69856062-1', 'auto');
      ga('send', 'pageview');
    </script>
</body>
</html>
