<div class="footer">
  <div class="container">
    @if(!isset($homeLink))
      <a href="{{ url('/') }}" class="home-btn btn"></a>
    @endif
    <div class="social-links">
        <a href="https://www.facebook.com/tropicotunisie" id="fb" target="_blank" class="fa fa-facebook"></a>
        <a href="https://www.youtube.com/channel/UCtzvi_jnR-_eGdnSUWRBg4g" id="yt" target="_blank" class="fa fa-youtube"></a>
    </div>
    <div class="copyright">Tous droits réservés &copy; 2015</div>
  </div>
</div>
