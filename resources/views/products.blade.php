@extends('layout')

@section('class', 'products')

@section('content')
<div id="products-section">
  <div class="container">
    <div class="products-plank"></div>
    <div class="slogan text-hide">إكتشف عالم الطيور مع كوكو و إربح رحلة أحلامك</div>
    <img class="tropico-products" src="{{ asset('img/tropico-products.png') }}" alt="">
  </div>

  @include('partials.footer')
</div>
@endsection
